/*
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HKS_CONFIG_FILE
#include HKS_CONFIG_FILE
#else
#include "hks_config.h"
#endif

#ifdef HKS_SUPPORT_KDF_C

#include "hks_mbedtls_kdf.h"

#include <mbedtls/hkdf.h>
#include <mbedtls/md.h>
#include <mbedtls/pkcs5.h>

#include "hks_log.h"
#include "hks_mbedtls_common.h"
#include "hks_template.h"
#include "hks_type_inner.h"

#ifdef _CUT_AUTHENTICATE
#undef HKS_SUPPORT_KDF_PBKDF2
#endif

#define SINGLE_PRINT_LENGTH 50

static char int_to_ascii(const uint8_t in_num)
{
    if (in_num <= 9)
        return (char)('0' + in_num);
    return (char)('A' + in_num - 10);
}

static int32_t buffer_to_ascii(const uint8_t *src, const uint32_t src_size, char *dst, uint32_t *dst_size)
{
    const uint32_t ascii_len = src_size * 2 + 1;
    if (*dst_size < ascii_len) {
        HKS_LOG_E("buffer is too small");
        return -1;
    }

    for (uint32_t i = 0; i < src_size; ++i) {
        dst[2 * i] = int_to_ascii(src[i] >> 4);
        dst[2 * i + 1] = int_to_ascii(src[i] & 0b00001111);
    }

    dst[ascii_len - 1] = '\0';
    *dst_size = ascii_len;
    return 0;
}

static void print_buffer(const uint8_t *buffer, const uint32_t buffer_size)
{
    printf("===== huks print_buffer size[%u]=====\n", buffer_size);
    uint32_t index = 0;
    const uint32_t print_count = buffer_size / SINGLE_PRINT_LENGTH;
    for (uint32_t i = 0; i < (print_count + 1); ++i) {
        char chars[SINGLE_PRINT_LENGTH * 2 + 1] = {0};
        uint32_t char_size = SINGLE_PRINT_LENGTH * 2 + 1;
        buffer_to_ascii(buffer + index, (i == print_count)?buffer_size%SINGLE_PRINT_LENGTH:SINGLE_PRINT_LENGTH, chars, &char_size);
        printf("buff[%u] size[%u]:%s",i,(char_size - 1)/2, chars);
        printf("\n");
        index += SINGLE_PRINT_LENGTH;
    }
}


#ifdef HKS_SUPPORT_KDF_PBKDF2
static int32_t DeriveKeyPbkdf2(const struct HksBlob *mainKey, const struct HksKeyDerivationParam *derParam,
    const mbedtls_md_info_t *info, struct HksBlob *derivedKey)
{
    mbedtls_md_context_t ctx;
    (void)memset_s(&ctx, sizeof(mbedtls_md_context_t), 0, sizeof(mbedtls_md_context_t));
    mbedtls_md_init(&ctx);
    printf("###GSY print main key:\n");
    print_buffer(mainKey->data, mainKey->size);
    printf("###GSY print mbedtls md info:\n");
    printf("###GSY print salt:\n");
    print_buffer(derParam->salt.data, derParam->salt.size);
    printf("###GSY print info:\n");
    print_buffer(derParam->info.data, derParam->info.size);
    printf("###GSY iterations: %u\n", derParam->iterations);
    printf("###GSY digestAlg: %u\n", derParam->digestAlg);
    int32_t ret;
    do {
        ret = mbedtls_md_setup(&ctx, info, 1); /* 1 for using HMAC */
        if (ret != HKS_MBEDTLS_SUCCESS) {
            HKS_LOG_E("Mbedtls md setup failed! mbedtls ret = 0x%" LOG_PUBLIC "X", ret);
            break;
        }

        ret = mbedtls_pkcs5_pbkdf2_hmac(&ctx, mainKey->data, mainKey->size, derParam->salt.data,
            derParam->salt.size, derParam->iterations, derivedKey->size, derivedKey->data);
        if (ret != HKS_MBEDTLS_SUCCESS) {
            HKS_LOG_E("Mbedtls pbkdf2 failed! mbedtls ret = 0x%" LOG_PUBLIC "X", ret);
            (void)memset_s(derivedKey->data, derivedKey->size, 0, derivedKey->size);
        }
    } while (0);
    printf("###GSY print derived key:\0");
    print_buffer(derivedKey->data, derivedKey->size);
    mbedtls_md_free(&ctx);
    return ret;
}
#endif /* HKS_SUPPORT_KDF_PBKDF2 */

#ifdef HKS_SUPPORT_KDF_HKDF
static int32_t DeriveKeyHkdf(const struct HksBlob *mainKey, const struct HksKeyDerivationParam *derParam,
    const mbedtls_md_info_t *info, struct HksBlob *derivedKey)
{
    int32_t ret = mbedtls_hkdf(info, derParam->salt.data, derParam->salt.size, mainKey->data, mainKey->size,
        derParam->info.data, derParam->info.size, derivedKey->data, derivedKey->size);
    if (ret != HKS_MBEDTLS_SUCCESS) {
        HKS_LOG_E("Mbedtls hkdf failed! mbedtls ret = 0x%" LOG_PUBLIC "X", ret);
        (void)memset_s(derivedKey->data, derivedKey->size, 0, derivedKey->size);
    }

    return ret;
}
#endif /* HKS_SUPPORT_KDF_HKDF */

int32_t HksMbedtlsDeriveKey(const struct HksBlob *mainKey,
    const struct HksKeySpec *derivationSpec, struct HksBlob *derivedKey)
{
    const struct HksKeyDerivationParam *derParam = (struct HksKeyDerivationParam *)(derivationSpec->algParam);

    uint32_t mbedtlsAlg;
    int32_t ret = HksToMbedtlsDigestAlg(derParam->digestAlg, &mbedtlsAlg);
    HKS_IF_NOT_SUCC_RETURN(ret, ret)

    const mbedtls_md_info_t *info = mbedtls_md_info_from_type((mbedtls_md_type_t)mbedtlsAlg);
    HKS_IF_NULL_LOGE_RETURN(info, HKS_ERROR_CRYPTO_ENGINE_ERROR,
        "Mbedtls get md info failed! mbedtls ret = 0x%" LOG_PUBLIC "X", ret)

    switch (derivationSpec->algType) {
#ifdef HKS_SUPPORT_KDF_PBKDF2
        case HKS_ALG_PBKDF2:
            return DeriveKeyPbkdf2(mainKey, derParam, info, derivedKey);
#endif
#ifdef HKS_SUPPORT_KDF_HKDF
        case HKS_ALG_HKDF:
            return DeriveKeyHkdf(mainKey, derParam, info, derivedKey);
#endif
        default:
            HKS_LOG_E("Unsupport derive key alg! mode = 0x%" LOG_PUBLIC "X", derivationSpec->algType);
            return HKS_ERROR_INVALID_ARGUMENT;
    }
}
#endif /* HKS_SUPPORT_KDF_C */
